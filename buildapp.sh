#!/usr/bin/env bash
#
# Package application under MacOS or Linux
#

os=
case "$(uname)" in
    Darwin)
        os="darwin"
        ;;
    Linux)
        os="linux"
        ;;
    *)
        echo "OS $(uname) is unsupported"
        exit 1
        ;;
esac

set -e
type go >&2
type fyne >&2

vers=$(git describe --tags --always --dirty --match=v* 2> /dev/null || \
	   cat $(pwd)/.version 2> /dev/null || echo v0)
go build -ldflags "-s -w -X main.Version=${vers} -X main.BuildDate=$(date +'%FT%T')"

fyne package -os "$os" -appVersion "${vers#v}" -executable ./tntapp
rm -f ./tntapp
