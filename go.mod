module tntapp

go 1.16

require (
	bitbucket.org/mfkenney/go-nmea v1.5.2
	bitbucket.org/uwaploe/tnt v0.1.0
	fyne.io/fyne/v2 v2.0.3
	github.com/albenik/go-serial v1.2.0
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07 // indirect
)

replace bitbucket.org/uwaploe/tnt => ../tnt
