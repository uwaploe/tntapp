package main

import (
	"strconv"

	"bitbucket.org/mfkenney/go-nmea"
)

// Heading contains the contents of a standard NMEA HDG sentence
type Heading struct {
	Hdg       float32 `json:"hdg"`
	Deviation float32 `json:"dev"`
	Variation float32 `json:"var"`
}

func (h *Heading) UnmarshalNMEA(s nmea.Sentence) error {
	x, err := strconv.ParseFloat(s.Fields[0], 32)
	if err != nil {
		return err
	}
	h.Hdg = float32(x)

	x, err = strconv.ParseFloat(s.Fields[1], 32)
	if err != nil {
		return err
	}
	if s.Fields[2] == "W" {
		h.Deviation = float32(-x)
	} else {
		h.Deviation = float32(x)
	}

	x, err = strconv.ParseFloat(s.Fields[3], 32)
	if err != nil {
		return err
	}
	if s.Fields[4] == "W" {
		h.Variation = float32(-x)
	} else {
		h.Variation = float32(x)
	}

	return nil
}

type Alarm string

const (
	CalAlarm     Alarm = "C"
	LowAlarm           = "L"
	LowWarning         = "M"
	Normal             = "N"
	HighWarning        = "O"
	HighAlarm          = "P"
	VoltageAlarm       = "H"
)

// Htm contains the contents of a TNT HTM NMEA sentence
type Htm struct {
	Hdg         float32
	MagStatus   Alarm
	Pitch       float32
	PitchStatus Alarm
	Roll        float32
	RollStatus  Alarm
	Dip         float32
	Hmag        float32
}

func (h *Htm) UnmarshalNMEA(s nmea.Sentence) error {
	x, err := strconv.ParseFloat(s.Fields[0], 32)
	if err != nil {
		return err
	}
	h.Hdg = float32(x)
	h.MagStatus = Alarm(s.Fields[1])

	x, err = strconv.ParseFloat(s.Fields[2], 32)
	if err != nil {
		return err
	}
	h.Pitch = float32(x)
	h.PitchStatus = Alarm(s.Fields[3])

	x, err = strconv.ParseFloat(s.Fields[4], 32)
	if err != nil {
		return err
	}
	h.Roll = float32(x)
	h.RollStatus = Alarm(s.Fields[5])

	x, err = strconv.ParseFloat(s.Fields[6], 32)
	if err != nil {
		return err
	}
	h.Dip = float32(x)

	x, err = strconv.ParseFloat(s.Fields[7], 32)
	if err != nil {
		return err
	}
	h.Hmag = float32(x)

	return nil
}
