// Simple GUI app to view the output from a TNT electronic compass
package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"time"

	"bitbucket.org/uwaploe/tnt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	serial "github.com/albenik/go-serial"
	pserial "github.com/tarm/serial"
)

const appId = "edu.uw.apl.tntapp"

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	devBaud   int = 19200
	rdTimeout     = time.Second * 3
	tntDev    *tnt.Device
	tntPort   *pserial.Port
)

func openDevice(devname string) error {
	if tntPort != nil {
		tntPort.Close()
	}

	var err error
	tntPort, err = pserial.OpenPort(&pserial.Config{
		Name:        devname,
		Baud:        devBaud,
		ReadTimeout: rdTimeout})
	if err != nil {
		return err
	}

	tntDev = tnt.NewDevice(tntPort)
	tntDev.EnableOutput()
	err = tntDev.SetRate(tnt.HTM, tnt.Rate60Pmin)

	return err
}

func selectPort(a fyne.App, w fyne.Window, cb func()) {
	names, err := serial.GetPortsList()
	if err != nil {
		log.Fatal(err)
	}

	devname := binding.NewString()

	plist := widget.NewSelect(names, func(name string) {
		devname.Set(name)
		a.Preferences().SetString("CompassPort", name)
	})
	plist.SetSelected(a.Preferences().StringWithFallback("CompassPort", "/dev/ttyUSB1"))

	dialog.ShowForm("Open serial port", "Open", "Cancel",
		[]*widget.FormItem{
			{Text: "Port", Widget: plist},
		},
		func(b bool) {
			name, err := devname.Get()
			if err != nil || name == "" {
				return
			}
			err = openDevice(name)
			if err != nil {
				dialog.ShowError(err, w)
				return
			}

			if cb != nil {
				cb()
			}
		}, w)
}

func updateData(ctx context.Context, vals map[string]binding.String) {
	ch, err_ch := tntDev.Stream(ctx)
	for s := range ch {
		if s.Id != "PTNTHTM" {
			continue
		}
		h := &Htm{}
		h.UnmarshalNMEA(s)

		vals["time"].Set(time.Now().Format(time.RFC3339))
		vals["heading"].Set(fmt.Sprintf("Heading: %.1f ° Mag", h.Hdg))
		vals["dip"].Set(fmt.Sprintf("Dip angle: %.1f °", h.Dip))
		vals["magh"].Set(fmt.Sprintf("MagH: %.1f nT", h.Hmag*5))
		vals["pitch"].Set(fmt.Sprintf("Pitch: %.1f °", h.Pitch))
		vals["roll"].Set(fmt.Sprintf("Roll: %.1f °", h.Roll))
	}

	log.Printf("error: %v", <-err_ch)
}

func main() {
	a := app.NewWithID(appId)
	w := a.NewWindow("TNT Compass Monitor")

	desc := []struct {
		name, label string
	}{
		{name: "time"},
		{name: "heading", label: "Heading:"},
		{name: "dip", label: "Dip angle:"},
		{name: "magh", label: "MagH:"},
		{name: "pitch", label: "Pitch:"},
		{name: "roll", label: "Roll:"},
	}

	vals := make(map[string]binding.String)
	data := container.NewVBox()
	for _, d := range desc {
		b := binding.NewString()
		if d.label != "" {
			b.Set(d.label)
		}
		vals[d.name] = b
		data.Add(widget.NewLabelWithData(b))
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var ctl *widget.Button
	ctl = widget.NewButton("Start", func() {
		ctl.SetText("Stop")
		ctl.OnTapped = func() {
			ctl.Disable()
			cancel()
		}
		go updateData(ctx, vals)
	})

	w.SetContent(container.NewVBox(
		data,
		layout.NewSpacer(),
		ctl,
	))
	w.Resize(fyne.NewSize(300, 200))

	if tntPort == nil {
		ctl.Disable()
		selectPort(a, w, func() { ctl.Enable() })
	}

	w.ShowAndRun()
}
